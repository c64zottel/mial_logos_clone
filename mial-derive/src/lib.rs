use proc_macro::TokenStream;

#[proc_macro_derive(Mial, attributes(mial, extras, error, end, token, regex))]
pub fn logos(input: TokenStream) -> TokenStream {
    mial_codegen::generate(input.into()).into()
}