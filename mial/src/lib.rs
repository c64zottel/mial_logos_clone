pub use mial_derive::Mial;

pub struct Lexrex {
    value: String,
}

pub trait Mial {
    fn lex() -> Lexrex {
        println!("lexing");
        Lexrex { value: "lexrexed".to_string() }
    }
}